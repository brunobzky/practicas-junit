package fechas;

import org.junit.Test;
import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;
import org.junit.Before;


public class PruebaFechas  {
    @Test public void BisiestoMcCabe(){
        assertTrue(Fechas.Bisiesto(1500));
        assertTrue(Fechas.Bisiesto(2000));
        assertFalse(Fechas.Bisiesto(1900));
        assertFalse(Fechas.Bisiesto(2011));
        assertTrue(Fechas.Bisiesto(2012));
    }
    
    public static junit.framework.Test suite(){
        return new JUnit4TestAdapter(PruebaFechas.class);
    }
}
