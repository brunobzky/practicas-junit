/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forma;

import org.junit.Test;
import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;
import org.junit.Before;
import forma.Forma;

/**
 *
 * @author brunobzky
 */
public class PruebaForma {
    
    Forma f = new Forma();
    
    @Test
    public void PruebaFormaValoresLimite(){
        assertFalse(f.presionarBoton("", "", ""));//Campos vacíos
        assertFalse(f.presionarBoton("a", "1", "1"));//nombre válido, importe inválido, teléfono inválido
        assertTrue(f.presionarBoton("abcdefghijklmnopqrstuvwxyzabcd", "1.0", "1234567890"));//nombre válido, importe válido, teléfono válido
        assertFalse(f.presionarBoton("abcdefghijklmnopqrstuvwxyzabcde", "1234567890.0", "12345678901")); //nombre inválido, importe válido, teléfono inválido
    }
    
    
    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(PruebaForma.class);
    }
}
