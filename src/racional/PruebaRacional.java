package racional;

import junit.framework.JUnit4TestAdapter;
import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class PruebaRacional {

    private Racional racional, racional1;

    @Before
    public void setUp() {
        racional = new Racional(2, 3, 3, 4);
        racional1 = new Racional(2, 0, 3, 4);
    }
    
    @Test
    public void RacionalConstructor(){
        assertTrue(racional.valida());
        assertFalse(racional1.valida());
    }
    
    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(PruebaRacional.class);
    }

}
