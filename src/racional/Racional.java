package racional;

import java.awt.HeadlessException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Racional extends JFrame {

    private JFrame frame;
    private JTextField num1, num2, de1, de2, r1, r2;
    private JComboBox<String> cbOperaciones;
    private String[] operaciones = {"Suma", "Resta", "Multiplicacion", "Division"};
    private JButton btnEjecutar;
    private int n1, n2, d1, d2;

    public Racional(int n1, int d1, int n2, int d2) throws HeadlessException {
        this.n1 = n1;
        this.n2 = n2;
        this.d1 = d1;
        this.d2 = d2;
        setSize(310, 130);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(3);
        setLayout(null);

        frame = new JFrame();
        frame.setBounds(600, 600, 600, 600);
        num1 = new JTextField();
        num1.setBounds(10, 10, 35, 25);
        de1 = new JTextField();
        de1.setBounds(10, 40, 35, 25);
        num2 = new JTextField();
        num2.setBounds(65, 10, 35, 25);
        de2 = new JTextField();
        de2.setBounds(65, 40, 35, 25);
        r1 = new JTextField();
        r1.setBounds(220, 10, 35, 25);
        r2 = new JTextField();
        r2.setBounds(220, 40, 35, 25);

        btnEjecutar = new JButton("=");
        btnEjecutar.setBounds(125, 40, 75, 25);

        cbOperaciones = new JComboBox<>(operaciones);
        cbOperaciones.setBounds(125, 10, 75, 25);
        

        frame.add(num1);
        frame.add(num1);
        frame.add(de1);
        frame.add(num2);
        frame.add(de2);
        frame.add(cbOperaciones);
        frame.add(r1);
        frame.add(r2);
        frame.add(btnEjecutar);
        
        frame.setVisible(true);
        pack();
        
    }

    public ArrayList<Integer> reducirFraccion(int num1, int num2) {
        ArrayList<Integer> aux = new ArrayList<>();
        ArrayList<Integer> f1 = new ArrayList<>();
        ArrayList<Integer> f2 = new ArrayList<>();

        f1 = mcd(num1);
        f2 = mcd(num2);

        if (f1.size() <= f2.size()) {//f2 es mayor
            int c1 = 0, c2 = 0;
            while (c1 < f2.size()) {
                if (f2.get(c1) == f1.get(c2)) {
                    num1 /= f1.get(c2);
                    num2 /= f2.get(c1);
                    c1++;
                    c2++;
                } else {
                    c1++;
                }
            }
        } else {//f1 es mayor
            int c1 = 0, c2 = 0;
            while (c2 < f1.size()) {
                if (f1.get(c2) == f2.get(c1)) {
                    num1 /= f1.get(c2);
                    num2 /= f2.get(c1);
                    c1++;
                    c2++;
                } else {
                    c2++;
                }
            }
        }
        aux.add(num1);
        aux.add(num2);

        return aux;
    }

    public ArrayList<Integer> mcd(int numero) {
        ArrayList<Integer> aux = new ArrayList<>();
        int cont = 2;
        int num = numero;

        if (numero > 1) {
            while (cont <= numero) {
                if (num != 1) {
                    if (num % cont == 0) {
                        aux.add(cont);
                        num /= cont;
                    } else {
                        cont++;
                    }
                } else {
                    break;
                }
            }
        } else {
            aux.add(1);
        }
        return aux;
    }

//    public ArrayList<Integer> suma(int n1, int d1, int n2, int d2) {
//        ArrayList<Integer> aux = new ArrayList<>();
//        aux.add((n1 * d2) + (n2 * d1));
//        aux.add(d1 * d2);
//        aux = reducirFraccion(aux.get(0), aux.get(1));
//        return aux;
//    }
    public ArrayList<Integer> suma() {
        ArrayList<Integer> aux = new ArrayList<>();
        aux.add((n1 * d2) + (n2 * d1));
        aux.add(d1 * d2);
        aux = reducirFraccion(aux.get(0), aux.get(1));
        return aux;
    }

    public ArrayList<Integer> resta(int n1, int d1, int n2, int d2) {
        ArrayList<Integer> aux = new ArrayList<>();
        aux.add((n1 * d2) - (n2 * d1));
        aux.add(d1 * d2);
        aux = reducirFraccion(aux.get(0), aux.get(1));
        return aux;
    }

    public ArrayList<Integer> multi(int n1, int d1, int n2, int d2) {
        ArrayList<Integer> aux = new ArrayList<>();
        aux.add(n1 * n2);
        aux.add(d1 * d2);
        aux = reducirFraccion(aux.get(0), aux.get(1));
        return aux;
    }

    public ArrayList<Integer> divi(int n1, int d1, int n2, int d2) {
        ArrayList<Integer> aux = new ArrayList<>();
        aux.add(n1 * d2);
        aux.add(n2 * d1);
        aux = reducirFraccion(aux.get(0), aux.get(1));
        return aux;
    }

    public boolean valida() {
        if (d1 == 0 | d2 == 0) {
            return false;
        }
        return true;
    }
    
    public static void main(String[] args) {
        Racional r = new Racional(1, 2, 1, 3);
        r.setVisible(true);
        System.out.println();
    }
}
