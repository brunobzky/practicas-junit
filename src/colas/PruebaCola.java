package colas;

import org.junit.Test;
import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;
import org.junit.Before;

public class PruebaCola {

    private Cola co;
    
    @Test
    public void ColaCircularEscenario() {
           
        co = new Cola(5);
        assertTrue(co.esVacia()); //Cola vacia
        assertEquals(co.cuantos(), 0); //Cola vacia
        assertFalse(co.esElemento(3)); //Cola vacia
        assertFalse(co.elimina(co.dato)); //Cola vacia
        assertEquals(co.indiceElemento(2), -1);
        assertTrue(co.inserta(90));     // Inserta      Cola Vacia
        assertEquals(co.cuantos(), 1);   // Despliega    Cola con elementos
        assertTrue(co.inserta(28));     // Inserta      Cola con elementos
        assertEquals(co.cuantos(), 2);   // Despliega    Cola con Elementos
        assertFalse(co.esVacia());      // EsVacia      No es Vacia
        assertTrue(co.inserta(60));     // Inserta      Cola con elementos
        assertEquals(co.indiceElemento(2), 28);  // IndiceElemento      Cola con elementos
        assertTrue(co.inserta(90));     // Inserta      Cola Vacia
        assertEquals(co.indiceElemento(3), 60);  // IndiceElemento      Elemento duplicado
        assertFalse(co.esLlena());      // EsLlena      No esta llena
        assertTrue(co.esElemento(60));  // EsElemento   El elemento existe
        assertFalse(co.esElemento(34)); // EsElemento   El elemento no existe
        assertTrue(co.esElemento(90));  // EsElemento   El elemento duplicado
        assertTrue(co.elimina(co.dato));   // Elimina      Elemento duplicado
        assertEquals(co.dato.getDato(), 90);     // Elimina      Elemento duplicado
        assertTrue(co.inserta(10));     // Inserta      
        assertTrue(co.inserta(100));    // Inserta      Ultimo elemento disponible
        assertTrue(co.esElemento(100)); // EsElemento   cola Llena
        assertTrue(co.esLlena());       // EsLlena      cola llena
        assertEquals(co.cuantos(), 5);   // Cuantos      cola llena; Despliega cola llena
        assertFalse(co.inserta(63));    // Inserta      cola llena
        assertTrue(co.elimina(co.dato));   // Elimina      cola llena
        assertEquals(co.dato.getDato(), 28);    // Elimina      cola con elementos
        assertTrue(co.elimina(co.dato));   // Elimina      cola con elementos
        assertEquals(co.dato.getDato(), 60);
        assertTrue(co.elimina(co.dato));   // Elimina      cola con elementos
        assertEquals(co.dato.getDato(), 90);
        assertTrue(co.elimina(co.dato));   // Elimina      cola con elementos
        assertEquals(co.dato.getDato(), 10);
        assertTrue(co.elimina(co.dato));   // Elimina      cola con elementos
        assertEquals(co.dato.getDato(), 100);
        assertFalse(co.elimina(co.dato));  // Elimina      cola Vacia
    }

    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(PruebaCola.class);
    }

}
