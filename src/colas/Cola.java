package colas;

public class Cola {

    int[] cola;
    int fondo, raiz;
    Dato dato;

    class Dato {

        int dato;

        public Dato() {
        }

        public int getDato() {
            return dato;
        }

        public void setDato(int dato) {
            this.dato = dato;
        }

    }

    public Cola(int tam) {
        cola = new int[tam];
        raiz = -1;
        fondo = -1;
        dato = new Dato();
        dato.dato = 0;
    }

    public boolean inserta(int num) {
        if (!esLlena()) {
            if (esVacia()) {
                raiz++;
                fondo++;
                cola[fondo] = num;
                return true;
            } else {
                fondo++;
                cola[fondo] = num;
                return true;
            }
        }
        return false;
    }

    public boolean elimina(Dato dato) {
        if (!esVacia()) {
            if (fondo == raiz) {
                dato.dato = cola[fondo];
                fondo = -1;
                raiz = -1;
                return true;
            } else {
                dato.dato = cola[raiz];
                for (int i = 1; i < fondo+1; i++) {
                    int aux = cola[i-1];
                    cola[i-1]=cola[i];
                }
                fondo--;
                return true;
            }
        } else {
            dato.dato = -1;
            return false;
        }
    }

    public boolean esVacia() {
        if (fondo == -1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean esLlena() {
        if (fondo + 1 == cola.length) {
            return true;
        } else {
            return false;
        }
    }

    public void despliega() {
        for (int i = 0; i < fondo + 1; i++) {
            System.out.println(cola[i]);
        }
    }

    public int cuantos() {
        return fondo + 1;
    }

    public boolean esElemento(int dato) {
        if (!esVacia()) {
            for (int i = 0; i < fondo + 1; i++) {
                if (dato == cola[i]) {
                    return true;
                }
            }
        }
        return false;
    }

    public int indiceElemento(int indice) {
        if (!esVacia() && indice > 0) {
            if (indice <= fondo + 1) {
                return cola[indice - 1];
            } else {
                return -1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Cola cola = new Cola(5);
    }

}
