package num_texto;

import practicas_junit.PracticasJUnit;

public class NumText {

    public static PracticasJUnit p = new PracticasJUnit();//Creando una instancia de la clase principal para recoger el numero
    public static String num = Long.toString(p.variable);//número long convertido a cadena de texto
    public static char[] arreglo = num.toCharArray();//Conversión de string a arreglo de caracteres
    public static StringBuilder sb = new StringBuilder();//Creación de la cadena que almacenará el texto en números

    public static  boolean numeroTexto(long numero) {
        invArray(arreglo);

        if (p.variable >= 1000000000000000000l && p.variable <= 9223372036854775807l) {
            generarUnidades(18);//UNIDADES DE TRILLÓN
            if (arreglo[18] == '1' && p.variable <= 1999999999999999999l) {
                sb.append("un trillón ");//BILLONES
            } else {
                sb.append("trillones ");
            }
            generarCentenas(17);//CENTENAS DE MILES DE BILLONES
            generarDecenas(16);//DECENAS DE MILES DE BILLONES
            generarUnidades(15);//UNIDADES DE MILES DE BILLONES
            if (arreglo[14] == '0' && arreglo[13] == '0' && arreglo[12] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(14);//CENTENAS DE BILLÓN
            generarDecenas(13);//DECENAS DE BILLÓN
            generarUnidades(12);//UNIDADES DE BILLÓN
            if (arreglo[12] == '1' && p.variable <= 1999999999999l) {
                sb.append("un billón ");//BILLONES
            } else {
                sb.append("billones ");
            }
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        } 

        if (p.variable < 100000000000000000l || p.variable > 999999999999999999l) {
        } else {
            generarCentenas(17);//CENTENAS DE MILES DE BILLONES
            generarDecenas(16);//DECENAS DE MILES DE BILLONES
            generarUnidades(15);//UNIDADES DE MILES DE BILLONES
            if (arreglo[14] == '0' && arreglo[13] == '0' && arreglo[12] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(14);//CENTENAS DE BILLÓN
            generarDecenas(13);//DECENAS DE BILLÓN
            generarUnidades(12);//UNIDADES DE BILLÓN
            if (arreglo[12] == '1' && p.variable <= 1999999999999l) {
                sb.append("un billón ");//BILLONES
            } else {
                sb.append("billones ");
            }
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 10000000000000000l && p.variable <= 99999999999999999l) {
            generarDecenas(16);//DECENAS DE MILES DE BILLONES
            generarUnidades(15);//UNIDADES DE MILES DE BILLONES
            if (arreglo[14] == '0' && arreglo[13] == '0' && arreglo[12] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(14);//CENTENAS DE BILLÓN
            generarDecenas(13);//DECENAS DE BILLÓN
            generarUnidades(12);//UNIDADES DE BILLÓN
            if (arreglo[12] == '1' && p.variable <= 1999999999999l) {
                sb.append("un billón ");//BILLONES
            } else {
                sb.append("billones ");
            }
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 1000000000000000l && p.variable <= 9999999999999999l) {
            generarUnidades(15);//UNIDADES DE MILES DE BILLONES
            if (arreglo[14] == '0' && arreglo[13] == '0' && arreglo[12] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(14);//CENTENAS DE BILLÓN
            generarDecenas(13);//DECENAS DE BILLÓN
            generarUnidades(12);//UNIDADES DE BILLÓN
            if (arreglo[12] == '1' && p.variable <= 1999999999999l) {
                sb.append("un billón ");//BILLONES
            } else {
                sb.append("billones ");
            }
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 100000000000000l && p.variable <= 999999999999999l) {
            generarCentenas(14);//CENTENAS DE BILLÓN
            generarDecenas(13);//DECENAS DE BILLÓN
            generarUnidades(12);//UNIDADES DE BILLÓN
            if (arreglo[12] == '1' && p.variable <= 1999999999999l) {
                sb.append("un billón ");//BILLONES
            } else {
                sb.append("billones ");
            }
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 10000000000000l && p.variable <= 99999999999999l) {
            generarDecenas(13);//DECENAS DE BILLÓN
            generarUnidades(12);//UNIDADES DE BILLÓN
            if (arreglo[12] == '1' && p.variable <= 1999999999999l) {
                sb.append("un billón ");//BILLONES
            } else {
                sb.append("billones ");
            }
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 1000000000000l && p.variable <= 9999999999999l) {
            generarUnidades(12);//UNIDADES DE MILLÓN
            if (arreglo[12] == '1' && p.variable <= 1999999999999l) {
                sb.append("un billón ");//MILLONES
            } else {
                sb.append("billones ");
            }
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 100000000000l && p.variable <= 999999999999l) {
            generarCentenas(11);//CENTENAS DE MILES DE MILLONES
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 10000000000l && p.variable <= 99999999999l) {
            generarDecenas(10);//DECENAS DE MILES DE MILLONES
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 1000000000 && p.variable <= 9999999999l) {
            generarUnidades(9);//UNIDADES DE MILES DE MILLONES
            if (arreglo[8] == '0' && arreglo[7] == '0' && arreglo[6] == '0') {
                sb.append("");//MILES DE MILLONES
            } else {
                sb.append("mil ");
            }
            generarCentenas(8);//CENTENAS DE MILLÓN
            generarDecenas(7);//DECENAS DE MILLÓN
            generarUnidades(6);//UNIDADES DE MILLÓN
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 100000000 && p.variable <= 999999999) {
            generarCentenas(8);
            generarDecenas(7);
            generarUnidades(6);
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 10000000 && p.variable <= 99999999) {
            generarDecenas(7);
            generarUnidades(6);
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("un millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 1000000 && p.variable <= 9999999) {
            generarUnidades(6);
            if (arreglo[6] == '1' && p.variable <= 1999999) {
                sb.append("un millón ");//MILLONES
            } else {
                sb.append("millones ");
            }
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            if (arreglo[5] == '0' && arreglo[4] == '0' && arreglo[3] == '0') {
                sb.append("");//MILLARES
            } else {
                sb.append("mil ");
            }
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 100000 && p.variable <= 999999) {
            generarCentenas(5);//CENTENAS DE MILLAR
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            sb.append("mil ");
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 10000 && p.variable <= 99999) {
            generarDecenas(4);//DECENAS DE MILLAR
            generarUnidades(3);//UNIDADES DE MILLAR
            sb.append("mil ");
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 1000 && p.variable <= 9999) {
            generarUnidades(3);//UNIDADES DE MILLAR
            sb.append("mil ");
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 100 && p.variable <= 999) {
            generarCentenas(2);//CENTENAS
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 10 && p.variable <= 99) {
            generarDecenas(1);//DECENAS
            generarUnidades(0);//UNIDADES
        }

        if (p.variable >= 0 && p.variable <= 9) {
            generarUnidades(0);//UNIDADES
        }
        System.out.println(sb);
        return true;
    }

    public static void generarUnidades(int pos) {

        switch (arreglo[pos]) {
            case '1':
                if (arreglo.length > (pos + 1) && arreglo[pos + 1] == '1') {
                    sb.append("");
                } else {
                    if (pos == 0) {//Si la cantidad termina en uno
                        sb.append("uno");
                    } else if (arreglo.length >= pos + 3 && arreglo[pos + 1] != '0' && arreglo[pos + 1] != '1') {//si la cantidad es mayor a mil pero necesita terminar en un
                        sb.append("un ");
                    } else {
                        sb.append("");
                    }
                }
                break;
            case '2':
                if (arreglo.length > (pos + 1) && arreglo[pos + 1] == '1') {//Si es doce no escribirá el dos
                    sb.append("");
                } else {
                    sb.append("dos ");
                }
                break;
            case '3':
                if (arreglo.length > (pos + 1) && arreglo[pos + 1] == '1') {//Si es trece no escribirá el tres
                    sb.append("");
                } else {
                    sb.append("tres ");
                }
                break;
            case '4':
                if (arreglo.length > (pos + 1) && arreglo[pos + 1] == '1') {//Si es catorce no escribirá el cuatro
                    sb.append("");
                } else {
                    sb.append("cuatro ");
                }
                break;
            case '5':
                if (arreglo.length > (pos + 1) && arreglo[pos + 1] == '1') {//Si es quince no escribirá el cinco
                    sb.append("");
                } else {
                    sb.append("cinco ");
                }
                break;
            case '6':
                sb.append("seis ");
                break;
            case '7':
                sb.append("siete ");
                break;
            case '8':
                sb.append("ocho ");
                break;
            case '9':
                sb.append("nueve ");
                break;
            case '0':
                if (arreglo.length > (pos + 1) && arreglo[pos + 1] == '1') {//Si es doce no escribirá el dos
                    sb.append("");
                } else {
                    if (arreglo.length == 1) {//Si la cantidad termina en uno
                        sb.append("cero");
                    }
                }
                break;
            default:
                break;
        }

    }

    public static void generarDecenas(int pos) {
        switch (arreglo[pos]) {
            case '1':
                switch (arreglo[pos - 1]) {
                    case '0':
                        sb.append("diez ");
                        break;
                    case '1':
                        sb.append("once ");
                        break;
                    case '2':
                        sb.append("doce ");
                        break;
                    case '3':
                        sb.append("trece ");
                        break;
                    case '4':
                        sb.append("catorce");
                        break;
                    case '5':
                        sb.append("quince");
                        break;
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        sb.append("dieci");
                        break;
                    default:
                        break;
                }
                break;
            case '2':
                if (arreglo[pos - 1] == '0') {
                    sb.append("veinte");
                } else {
                    sb.append("veinti");
                }
                break;
            case '3':
                sb.append("treinta ");
                if (arreglo[pos - 1] != '0') {
                    sb.append("y ");
                }
                break;
            case '4':
                sb.append("cuarenta ");
                if (arreglo[pos - 1] != '0') {
                    sb.append("y ");
                }
                break;
            case '5':
                sb.append("cincuenta ");
                if (arreglo[pos - 1] != '0') {
                    sb.append("y ");
                }
                break;
            case '6':
                sb.append("sesenta ");
                if (arreglo[pos - 1] != '0') {
                    sb.append("y ");
                }
                break;
            case '7':
                sb.append("setenta ");
                if (arreglo[pos - 1] != '0') {
                    sb.append("y ");
                }
                break;
            case '8':
                sb.append("ochenta ");
                if (arreglo[pos - 1] != '0') {
                    sb.append("y ");
                }
                break;
            case '9':
                sb.append("noventa ");
                if (arreglo[pos - 1] != '0') {
                    sb.append("y ");
                }
                break;
            default:
                break;
        }
    }

    public static void generarCentenas(int pos) {
        switch (arreglo[pos]) {
            case '1':
                if (arreglo[pos - 1] == '0' && arreglo[pos - 2] == '0') {
                    sb.append("cien ");
                } else {
                    sb.append("ciento ");
                }
                break;
            case '2':
                sb.append("doscientos ");
                break;
            case '3':
                sb.append("trescientos ");
                break;
            case '4':
                sb.append("cuatrocientos ");
                break;
            case '5':
                sb.append("quinientos ");
                break;
            case '6':
                sb.append("seiscientos ");
                break;
            case '7':
                sb.append("setecientos ");
                break;
            case '8':
                sb.append("ochocientos ");
                break;
            case '9':
                sb.append("novecientos ");
                break;
        }
    }
    public static char[] invArray(char[] n) {
        int aux;
        for (int i = 0; i < n.length / 2; i++) {
            aux = n[i];
            n[i] = n[n.length - 1 - i];
            n[n.length - 1 - i] = (char) aux;
        }
        return n;
    }
}