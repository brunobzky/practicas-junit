package num_texto;

import org.junit.Test;
import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;
import org.junit.Before;
import num_texto.NumText;

public class PruebaNumText {
    
    @Test
    public void PruebaNumTextCondicionesMultiples(){
        assertTrue(NumText.numeroTexto(99999999999999999l));
        assertTrue(NumText.numeroTexto(1000000000000000000l));
    }
    
    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(PruebaNumText.class);
    }
    
}
