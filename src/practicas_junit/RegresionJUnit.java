package practicas_junit;

import fechas.PruebaFechas;
import triangulo.PruebaTriangulo;
import num_texto.PruebaNumText;
import limpia_parabrisas.PruebaLimpiaParabrisas;
import forma.PruebaForma;
import colas.PruebaCola;
import racional.PruebaRacional;

public class RegresionJUnit {
    public static void main(String[] args) {
       junit.textui.TestRunner.run(PruebaFechas.suite());
       
       junit.textui.TestRunner.run(PruebaTriangulo.suite());
       
       junit.textui.TestRunner.run(PruebaNumText.suite());
       
       junit.textui.TestRunner.run(PruebaLimpiaParabrisas.suite());
       
       junit.textui.TestRunner.run(PruebaForma.suite());
       
       junit.textui.TestRunner.run(PruebaCola.suite());
       
       junit.textui.TestRunner.run(PruebaRacional.suite());
    }
}