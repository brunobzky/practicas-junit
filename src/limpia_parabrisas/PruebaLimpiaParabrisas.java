package limpia_parabrisas;

import org.junit.Test;
import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;
import org.junit.Before;
import limpia_parabrisas.LimpiaParabrisas;


public class PruebaLimpiaParabrisas {
    
    @Test
    public void PruebaLimpiaParabrisasTablaCondiciones(){
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(0, 1), 0);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(0, 2), 0);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(0, 3), 0);
        
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(1, 1), 6);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(1, 2), 12);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(1, 3), 20);
        
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(2, 1), 30);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(2, 2), 30);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(2, 3), 30);

        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(3, 1), 60);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(3, 2), 60);
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(3, 3), 60);
        
        assertEquals(LimpiaParabrisas.simulaLimpiaParabrisas(6, 8), -1);
    }
    
    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(PruebaLimpiaParabrisas.class);
    }
    
}
