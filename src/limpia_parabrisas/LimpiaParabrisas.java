package limpia_parabrisas;

import java.util.Scanner;

public class LimpiaParabrisas {


    public static int simulaLimpiaParabrisas(int valor, int intermitente) {


        switch (valor) {
            case 0:
                return 0;
            case 1:

                switch (intermitente) {
                    case 1:
                        return 6;
                    case 2:
                        return 12;
                    case 3:
                        return 20;
                    default:
                        return -1;
                }
            case 2:
                return 30;
            case 3:
                return 60;
            default:
                return -1;
        }
    }

}
