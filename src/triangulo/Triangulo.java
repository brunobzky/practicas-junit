package triangulo;

public class Triangulo {

    public static int tipoTriangulo(int a, int b, int c) { //1

        if ((a >= (b + c)) || (b >= (a + c)) || (c >= (a + b)) || a <= 0 || a <=0 || c <=0) {
//            System.out.println("No es un triángulo");
            return 4;
        } else if ((a == b) && (b == c)) { //2
//            System.out.println("Equilátero"); //3
            return 1;
        } else //4
        if (((a == b) && (b != c)) || ((b == c) && (c != a)) || ((a == c) && (c != b))) { //5
//            System.out.println("Isósceles"); //6
            return 2;
        } else {//7
//            System.out.println("Escaleno"); //8
            return 3;
        }

    }
}
