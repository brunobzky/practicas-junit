package triangulo;

import org.junit.Test;
import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;
import org.junit.Before;

public class PruebaTriangulo {

    @Test
    public void PruebaTrianguloMcCabe() {
        assertEquals(Triangulo.tipoTriangulo(1, 1, 1), 1);
        assertEquals(Triangulo.tipoTriangulo(2, 2, 3), 2);
        assertEquals(Triangulo.tipoTriangulo(2, 3, 4), 3);
    }

    @Test
    public void PruebaTrianguloValoresLimiteSimple() {
        assertEquals(Triangulo.tipoTriangulo(100, 100, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 2), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 100), 1);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 200), 4);

        assertEquals(Triangulo.tipoTriangulo(100, 1, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 2, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 100), 4);

        assertEquals(Triangulo.tipoTriangulo(1, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(2, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 100), 4);
    }
    
    @Test
    public void PruebaTrianguloValoresLimiteSimplePeorCaso() {
        assertEquals(Triangulo.tipoTriangulo(100, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 2), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 100), 1);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 201), 4);

        assertEquals(Triangulo.tipoTriangulo(100, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 2, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 201, 100), 4);

        assertEquals(Triangulo.tipoTriangulo(-1, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(2, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 100, 100), 4);
    }

    @Test
    public void PruebaTrianguloValoresLimiteRobusto() {
        assertEquals(Triangulo.tipoTriangulo(0, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 1), 1);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 100), 1);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 199), 1);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 200), 1);
    }

    @Test
    public void PruebaTrianguloValoresLimiteRobustoPeorCaso() {
        assertEquals(Triangulo.tipoTriangulo(-1, -1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, -1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, -1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, -1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, -1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, -1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 0, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 0, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 100, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 100, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 199, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 199, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 199, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 199, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 199, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 200, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 200, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 200, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 200, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 201, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 201, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 201, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 201, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 201, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 201, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(-1, 201, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(0, -1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, -1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, -1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, -1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, -1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, -1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 0, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 100, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 199, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 200, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 201, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 201, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 201, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 201, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 201, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 201, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(0, 201, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(1, -1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, -1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, -1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, -1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, -1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, -1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 0, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 1), 1);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 100, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 199, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(1, 200, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 201, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 201, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 201, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 201, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 201, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 201, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(1, 201, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(100, -1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, -1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, -1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, -1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(100, -1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, -1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 0, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 100), 1);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 100, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 199, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 199, 201), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 200, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(100, 200, 201), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 201, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 201, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 201, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 201, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(100, 201, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 201, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(100, 201, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(199, -1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, -1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, -1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(199, -1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(199, -1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(199, -1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 0, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 100, 201), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 199, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 199), 1);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 199, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 200, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 200, 201), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 201, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 201, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 201, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(199, 201, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 201, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(199, 201, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(199, 201, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(200, -1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, -1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, -1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(200, -1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(200, -1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(200, -1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 0, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 100, 201), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 199, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 199, 201), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 200, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 200), 1);
        assertEquals(Triangulo.tipoTriangulo(200, 200, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 201, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 201, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 201, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(200, 201, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 201, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(200, 201, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(200, 201, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(201, -1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, -1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(201, -1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, -1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(201, -1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(201, -1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(201, -1, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 0, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 0, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 0, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 0, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 0, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 0, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 0, 201), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 1, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 1, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 1, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 1, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 1, 199), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 1, 200), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 1, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 100, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 100, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 100, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 100, 100), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 100, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(201, 100, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(201, 100, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 199, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 199, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 199, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 199, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(201, 199, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 199, 200), 3);
        assertEquals(Triangulo.tipoTriangulo(201, 199, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 200, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 200, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 200, 1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 200, 100), 3);
        assertEquals(Triangulo.tipoTriangulo(201, 200, 199), 3);
        assertEquals(Triangulo.tipoTriangulo(201, 200, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 200, 201), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 201, -1), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 201, 0), 4);
        assertEquals(Triangulo.tipoTriangulo(201, 201, 1), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 201, 100), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 201, 199), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 201, 200), 2);
        assertEquals(Triangulo.tipoTriangulo(201, 201, 201), 1);
    }

    public static junit.framework.Test suite() {
        return new JUnit4TestAdapter(PruebaTriangulo.class);
    }
}
